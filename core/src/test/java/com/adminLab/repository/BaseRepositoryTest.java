package com.adminLab.repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.Statement;
import com.adminLab.common.PropertyManager;
import com.adminLab.db.datasources.DataSourceFactory;
import com.adminLab.db.datasources.DataSourceFactoryImpl;
import com.adminLab.db.queryagent.QueryAgent;
import com.adminLab.db.queryagent.QueryAgentFactoryImpl;
import com.adminLab.environments.EnvironmentFactory;
import com.adminLab.environments.EnvironmentFactoryImpl;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;

public class BaseRepositoryTest {

    private static QueryAgent _queryAgent;

    static {
        PropertyManager.reloadConfiguration();
        loadQueryAgent();
    }

    private static void loadQueryAgent() {
        DataSourceFactory dataSourceFactory = new DataSourceFactoryImpl();
        EnvironmentFactory environmentFactory = new EnvironmentFactoryImpl();
        _queryAgent = new QueryAgentFactoryImpl(dataSourceFactory).getQueryAgent(environmentFactory.getUnitTestEnvironment());
    }

    @Before
    public void setUpBase() throws Exception {
        _queryAgent.beginTransaction();
    }

    @After
    public void tearDownBase() throws Exception {
        _queryAgent.rollbackTransaction();
    }

    protected void insertFixture(String fixturesFileName) throws Exception {
        final String fixturesDirectory = PropertyManager.getProperty("unit_test.fixture_dir");
        File file = new File(fixturesDirectory + fixturesFileName);
        executeFixtureFile(file);
    }

    protected QueryAgent getQueryAgent() {
        return _queryAgent;
    }

    private void executeFixtureFile(File scriptFile) throws Exception {
        if (scriptFile.exists() && scriptFile.isFile()) {
            String sql = FileUtils.readFileToString(scriptFile, "UTF-8");
            executeSql(sql, _queryAgent.getConnection());
        } else {
            throw new FileNotFoundException();
        }
    }

    private void executeSql(String sql, Connection conn) throws Exception {
        Statement st;
        st = conn.createStatement();
        st.execute(sql);
        st.close();
    }

    protected String encryptForSelect(String column, String wordToEncrypt) {
        return "crypt('" + wordToEncrypt + "', " + column + ")";
    }
}
