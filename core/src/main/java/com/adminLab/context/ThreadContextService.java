package com.adminLab.context;

import com.adminLab.db.queryagent.QueryAgent;
import com.adminLab.environments.Environment;

public interface ThreadContextService {

    void initializeContext(Environment env, String language);

    ThreadContext getThreadContext();

    QueryAgent getQueryAgent();

    void setThreadContextOnThread(ThreadContext threadContext);
}
