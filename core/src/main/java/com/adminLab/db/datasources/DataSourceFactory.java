package com.adminLab.db.datasources;

import javax.sql.DataSource;
import com.adminLab.environments.Environment;

public interface DataSourceFactory {
    DataSource getDataSource(Environment environment);
}
