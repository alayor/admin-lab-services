package com.adminLab.db.queryagent;

import com.adminLab.environments.Environment;

public interface QueryAgentFactory {
    QueryAgent getQueryAgent(Environment environment);
}
