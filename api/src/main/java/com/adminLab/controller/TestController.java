package com.adminLab.controller;

//import com.lealpoints.controller.base.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping(method = GET)
    public String test() {
        return "AdminLab API works! :)";
    }
}