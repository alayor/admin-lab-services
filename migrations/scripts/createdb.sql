DROP DATABASE IF EXISTS adminlab;
CREATE DATABASE adminlab;

DROP DATABASE IF EXISTS adminlab_unit_test;
CREATE DATABASE adminlab_unit_test;

DROP DATABASE IF EXISTS adminlab_functional_test;
CREATE DATABASE adminlab_functional_test;

